## A propos de moi

![moi](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/small.jpg)

Bonjour, je m'appelle Flore. Je suis actuellement en première **master en architecture**. 


### Mon parcours

Je suis née et ai grandi à Bruxelles. Après y avoir fait toute ma scolarité, je suis partie un an à l'étranger où j'ai approfondi mon anglais et suivi des cours de business au _Martin College Brisbane_. En rentrant en Belgique, je me suis lancée dans des études d'architecture à l'_ULB_. 

### Mes compétences

Au niveau personnel, je m'adapte facilement à la situation qui m'entoure, j'apprends rapidement et lorsque quelque chose me motive, j'y mets toute mon énergie.
Actuellement, je maitrise les bases quelques programmes tel que Autocad, SketchUp, Photoshop, InDesign et Illustrator. Avec cette option, j’espère acquérir de nouvelles compétences dans le domaine numérique et être capable d’utiliser les différents outils du FabLab.

## Mon choix 

![garden egg chair](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/42afd4bb5d213d645978966c9a9f69acc1bf562b/docs/images/660.jpg)

L’objet que j’ai choisi est la **Garden Egg chair** de Peter Ghyczy. Ce qui m’a attiré au premier regard est sa forme arrondie, sa couleur vive et son côté modulaire. En effet, cette assise peut se replie sur elle-même et ne laisser apparaitre plus qu’une forme d’œuf. Je trouve intéressant le paradoxe entre le fait que cet objet ait l’air d’être transportable par son fonctionnement mais également lourd et peu pratique à manipuler. Néanmoins je m’imagine bien le prendre avec moi lors d’un petit évènement en extérieur. J’y vois également plusieurs fonctions, servant d’assise lors d'un moment de détente et de table basse lorsqu'elle n'est pas utilisée. 

### Caractéristiques 

![dimensions](https://image.architonic.com/img_pro2-4/111/8911/icons-gardeneggchair-maten-z.jpg)

* **Designer:** Peter Ghyczy  
* **Date de création:** 1968  
* **Producteur:** VEB Synthesewerk Schwarzheide (1968-1998)  
* **Matériaux:** Polyurethane et tissu ou cuir






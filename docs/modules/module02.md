# Module 2: Fusion 360

Cette deuxième semaine au FabLab, nous nous sommes familiarisés avec l'impression 3D. Dans un premier temps, nous avons modélisé en 3D l'objet que nous avions choisi lors de la visite du _Design Museum Brussels_. Pour se faire, j'ai utlisé **Fusion 360**. 

## Modéliser avec Fusion 360 
J'ai eu plusieurs difficultées à modéliser mon objet, [the garden egg chair](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/660.jpg) de Peter Ghyczy. A plusieurs reprises, j'ai demandé de l'aide aussi bien aux assistantes, Gwen et Hélène, qu'au prof ou d'autres étudiants.
Il est important de bien réfléchir à la façon de modéliser l'objet avant de se lancer. J'ai pu recommencer plusieurs fois pour cette raison; j'ai essayé avec différentes approches et me retrouvais bloquée après. Afin de pouvoir produire cette semaine et ne pas accumuler du retard, j'ai du simplifié mon objet.   
  
A l'origine, il a une forme d'oeuf ellipsoïdal. J'ai réussi à créer cette forme mais seulement avec l'outil _Forme_ (cela produit un object vide). 
Par la suite, j'avais des difficultés à sculter la forme étant donné que cela créait des trous. Bien qu'ayant regardé plusieurs tutoriels, je n'ai pas su trouver de solution. De plus je ne savais pas si la forme devait être pleine pour pouvoir l'imprimer en 3D et donc choisir un remplissage ou si elle était vide, s'il était possible d'ajouter des supports internes.   
  
 ![modéle ellipsoïdal](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/Capture_d_%C3%A9cran_2020-10-21_150819.png)
   
J'ai donc choisi de simplifier ma forme en lui donnant une forme circulaire. De cette manière elle était pleine. Voici les grandes **étapes de ma modélisation**:

  **1.** importer une image afin de créer une esquisse du profil
![image importé profil](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/Capture_d_%C3%A9cran_2020-10-21_165321.png)  

 **2.** révolution du profil autour de l'axe central
 ![révolution](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/Capture_d_%C3%A9cran_2020-10-21_155515.png) 

 **3.** extruction au niveau de l'assise 
 ![extruction](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/Capture_d_%C3%A9cran_2020-10-21_171846.png)  

 **4.** extruction système d'ouverture  
 Afin d'imprimer l'objet facilement, j'ai simplifié le système d'ouverture en créant une fente d'ans l'assise afin d'y placer le couvercle. 
 ![système d'ouverture](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/Capture_d_%C3%A9cran_2020-10-21_172009.png)
  **5.** exportation
  J'ai exporté ma chaise en deux fois, l'assise et le couvercle séparément. Il est nécessaire d'exporter le fichier en **.stl**.

## Modélisation: deuxième tentative
J'ai essayé de reproduire cette fois-ci la _garden egg chair_ le plus fidèlement possible. J'ai donc recommencé une modélisation depuis le début. Il m'aura fallu du temps et beaucoup de manipulations, d'aller-retours pour y arriver. Je ne vais pas expliquer toutes les étapes inutiles que j'ai pu faire, mais me contenter de résumer celles qui ont été nécessaires. Afin que mon explication soit plus claire, je l'ai divisée en trois parties. 

### Assise
Lors de la première modélisation, je me suis rendue compte qu'il été compliqué de créer une forme ellipsoïdale. Le problème étant une autoconcurance du corps. Je me suis demandée si je pouvais la créer avec l'outil _Forme_, c'est-à-dire vide et ensuite la rendre sollide. J'ai fini par trouver le tutoriel [_Combine Sculpt To Model Work-space Important!_](https://www.youtube.com/watch?v=ngNjRSaVlvs&ab_channel=LarsChristensen) qui a rendu ça possible.

**1.** Import d'une image pour créer l'esquisse du profil.  
![import image + profil](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_1.1_small.png)

**2.** Balayage     
Avec l'outil balayge de Forme, j'ai pu créer l'envellope de ma forme. Cependant, il y a avait un problème aux niveaux des faces planes.
![balayage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_2.1_small.png)  
Je les ai donc supprimées.  
![supression des faces](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_2_small.png)

**3.** Fermeture face  
J'ai remplacé les deux faces qui me posaient problème.
![fermeture face](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_3_small.png)

**4.** Coudre   
Cette outil m'a permis de seléctionner les différentes faces de mon modèle pour les associer afin de créer un solide.  
![coudre](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_4_small.png)

**5.** Esquisse   
Dessiner le bord avant de l'assise.  
![esquisse](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_5_small.png)

**6.** Extrusion   
![extrusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_6_small.png)

**7.** Scinder une face & extrusion    
J'avais besoin de surélever la partie arrière de l'assise pour y placer le mécanisme d'ouverture. J'ai donc esquissé un trait qui va me servir à couper la face.  
![scinder une face](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_7_small.png)

### Couvercle
J'ai esssayé différentes méthodes pour créer la partie supérieure de l'assise, le dossier, le couvercle. Il n'était pas possible d'utiliser l'outil _Forme_ comme précédemment car il aurait toujours des problèmes d'autoconcurance. Je ne peux pas supprimer et recréer des faces arrondies aussi facilement. Voici celle qui, selon moi, arrivait au meilleur résultat.

**8.** Balayage
J'ai effectué un balayage d'une partie du profil du couvercle, avec un quart de l'ellipse comme cheminement. Il était impossible d'avoir un profil complet ou de faire un tour complet pour cause d'autoconcurance de la matière.  
![balayage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_8_small.png)

**9.** Symétrie miroir 
Afin d'avoir l'ensemble de la forme, j'ai fait deux symétrie. J'ai ensuite employé l'outil _combiner_ pour que c'est quatre quarts ne forme plus qu'un seul corps.  
![symétrie](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_9_small.png)

**10.** Esquisse & extrusion
J'ai utilisé ces outils afin de combler le trou au sommet.  
![extrusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_10_small.png)

**11.** J'ai repris la partie de l'avant de l'assise que j'avais précédemment coupée. 
![corps](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_11_small.png)  
Il faut maintanant que l'intériaur soit lisse, que le couvercle ait une épaisseur uniforme.

**12.** Esquisse
J'ai donc dessiné le profil de l'intérieur du couvercle, en vu de soustraire ce volume à la fome présente.   
![esquisse](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_12_small.png)

**13.** Balayage 
Avec l'outil de l'onglet en surface, j'ai balayé ce dernier profil avec comme chemin, toujours la même ellipse.   
![balayage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_13_small.png)  
J'ai également supprimer la face inférieure de cette coque.

**14.** Remplacer les faces
Cet outil m'a permis de changer les faces du couvercle actuel par celle de la fome dernièrement créée.  
![remplacer les faces](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_14_small.png)  
Voici le résulat:  
![lisse](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_14.2_small.png)

### Mécanisme d'ouverture

![photo mécanisme](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_m%C3%A9canisme_photo.jpg)  
Avant de trouver cette photo de la chaise sans coussin, montrant le mécanisme; j'étais partie sur une articulation toute autre. Cette image m'a permis de comprendre le fonctionnement de cet objet et de le modéliser correctement. 

**15.** Esquisse de l'attache de l'assise. 
![esquisse](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_15_small.png)  

**16.** Balayage   
![balayage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_16_small.png)

**17.** Esquisse & extrusion 
Ces deux fonctions ont permis de concevoir les attaches du couvercle. Néanmoins, ces dernières ressortent.  
![extruction](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_17_small.png)

**18.** Scinder le corps
Les attaches au niveau de leurs intesections avec la face interne du couvercle ont été coupées.  
![scinder](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_18_small.png)  
Les parties non utiles sont dès lors rendues invisibles.  
![résultat](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_18.2_small.png)  

**19.** Scinder le corps
Les parties des attaches du couvercle qui sont en contact avec la barre, sont enlevées afin de rendre l'articulation possible.
![scinder](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_19_small.png)  

**20.** Appuyer/Tirer
Les ouvertures sont élargies afin que la barre puisse bouger facilement. 
![appuyer/tirer](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_20_small.png)
![résultat](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_20.2_small.png)
  
Après avoir réfléchi à l'impression de cet objet, j'ai préféré le diviser en deux parties. En l'imprimant en une seule pièce, je pense que la partie infériere et supérieure n'airaient fait qu'un. C'est pour assurer l'articulation que j'ai séparé l'objet.
  
**21.** Esquisse & extruction
Création d'une ouverture.
![extruction](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_21_small.png)
![résultat](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_21.2_small.png)

**Vue d'ensemble**  
![final](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mod%C3%A9lisation_final_small.png)

**22.** Exportation
J'ai exporté ma chaise en deux fois, l'assise et le couvercle en deux fichiers **.stl** distincts. Il ne reste plus qu'à imprimer.



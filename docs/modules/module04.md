# Module 4: Découpe assistée par ordinateur

Cette semaine, nous avons eu une formation sur la découpe laser. Dans un premier temps, nous avons eu une présentation sur comment utiliser les machines. Dans un deuxième temps, nous avons eu comme exercice de créer un luminaire à partir d'une feuille de polypropylène et d'utliser le laser cut pour la découpe.

## Formation
Voici un petit résumé de la formation à la découpe et gravure au laser que nous avons eu.  

### Matériaux
Plusieurs types de matériaux peuvent être employés:
  * matériaux recommandés:  
    * bois contreplaqué max.3-4mm (plywood/multiplex)
    * acrylique (PMMA/Plexiglass)
    * papier/carton
    * certains textiles
  * matériaux déconseillés  
    Ceux-ci produisent de la fumée ou poussière nocive ou fondent facilement.
    * MDF
    * ABS,PS
    * PE,PET,PP 
    * composites à bas de fibres
    * métaux (impossible à couper)
  * matériaux interdits  
    Ceux-ci peuvent nuire à votre santé et/ou endommager la machine.
    * PVC: fumée acide et très nocive
      !!! ne pas confendre avec du plexi
    * cuivre: réfléchit totalement le laser
    * téflon:fumée acide et très nocive
    * vinyl, simili-cuir: peut contenir de la chlorine
    * résine phénolique, époxy: fumée très nocive
  
### Machines
Au FabLab de l'ULB, il y a deux découpeuses laser de différentes dimensions. 
#### Lasersaur: open-source
 * spécifications: 
   * surface de découpe: 122 x 61cm 
   * hauteur max: 12cm
   * vitesse max: 6000mm/min mais mieux vaut se limiter à 2000mm/min
 * précautions: 
   * connaître le matériau employé
   * ouvrir la vanne d'air comprimé
   * allumer l'extracteur de fumée
   * savoir où est le bouton d'arrêt d'urgence
   * savoir où est l'extincteur au CO2 le plus proche
 * interface : DriveboardApp 
   Les seuls fichiers pris en charge sont les vectoriels  SVG (utiliser des couleurs RVB) ou DXF (plus compliqué).  
#### Muse FullSpetrum
 * spécifications: 
   * surface de découpe: 50x30cm
   * hauteur max: 6cm
 * précaution à prendre:
   * connaître le matériau employé
   * brancher la pompe à air
   * allumer l'extracteur de fumée
   * savoir où est le bouton stop et pause
   * savoir où est l'extincteur au CO2 le plus proche
 * interface: Retina Engrave  
 Plusieurs formats sont pris en charge, des vectoriels et matriciels.
   * fichiers vectoriels : SVG (un fichier matriciel sera également importé, il faut supprimer un des deux)
   * images matricielles: BMP, JPEG, PNG, TIFF
   * PDF
#### Connection aux machines
Pour Lasersaur, un ordinateur y est connecté.  
Pour Muse FullSpetrum, 
  * via au wi-fi: LaserCutter
  * mot de passe: fablabULB2019
  * adresse: http://192.168.1.4

## Conception

L'exercice est donc de créer un abat-jour en 3D, en le dessinant en 2D et en l'assemblant de quelque manière que ce soit.  
J'ai commencé par faire une liste de toutes les idées, mots qui me venaient à l'esprit en pensant à mon objet, [_the garden egg chair_](https://www.ghyczy.com/collection/seating/garden-egg-chair/). De là, j'en ai retenu quelques-uns: articulation / ouverture / arrondi.
Dans un premier temps, j'ai travaillé par croquis et ensuite via des prototypes papiers. J'ai effectué plusieurs test afin de trouver les bonnes proportions pour avoir l'effet escompté. Voici les différents modèles, la troisième tentative fut la bonne.

 ![prototypes papier](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/lampepapier.JPG)


## Dessin 2D 
Pour pouvoir employer la découpeuse laser, il faut avoir un **fichier vectoriel .svg**.    

Premièrement, j'ai dessiner mon abat-jour sur **AutoCAD**. Il est nécessaire d'utiliser deux couleurs si l'on souhaite découper et graver. J'ai donc choisi du jaune pour la découpe et du bleu pour la gravure. J'ai ensuite exporter le fichier en **pdf**.  

 ![dessin autoCAD](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/lampe_autocad_small.png)  

Deuxièmement, j'ai ouvert le pdf dans **Illustrator**. J'ai utilisé ce logiciel uniquement afin de transformer le pdf en **svg**. Il est possible de directement dessiner dans Illustrator mais utilisant d'avantage AutoCAD, j'ai préféré faire ainsi.  

Dernièrement, il faut passer le fichier sur **InkScape** afin de vérifier que tout est bon. Personnelement, je ne l'ai pas téléchargé sur mon pc mais ai employé celui du FabLab connecté à la découpeuse. Por se faire, j'ai simplement importer mon fichier .svg via clé USB.
 Illustrator a tendance à grouper tous les traits, il est donc nécessiare de défaire le groupe. Pour ça, il suffit de selctionner l'ensemble des traits, faire un clic droit et cliquer sur **ungroup**. Sauvegarder le fichier, toujours en .svg.  
Le fichier créé peut enfin être ouvert par le logiciel de la découpeuse laser.

## Découpe laser 

### Mise en marche
Pour commencer, j'ai allumé la découpeuse laser **Lasersaur** via le bouton rouge. Celui-ci est également le bouton d'arrêt d'urgence. J'ai également ouvert la vanne d'air comprimé et enclenché l'extrateur de fumée et le refroidisseur.
J'ai ouvert le convercle de la machine pour y placer la feuille de polypropylène.  

### Paramètres & lancement
Ensuite, j'ai ouvert mon fichier avec l'interface **Driveboordapp**. 
J'ai  paramètré les **passages du laser**. Pour le premier passage, il faut  seléctionner celui de la gravure, en bleu chez moi; pour le deuxième, celui de la découpe, pour moi le jaune. Il est important de faire ça dans cet ordre là afin que le support coupé ne bouge pas.   
La vitesse de coupe,**F**, est en mm/min et la puissance **P** en %. Afin de savoir quelles données rentrées, c'est bien de partir de valeurs connues qui dépendent des matériaux et par une table de trois calculer celles que l'on souhaite. J'ai encodé une vitesse de 1500 pour une puissance de 46 % pour couper. Pour la gravure, il faut prendre plus ou moins la moitié de la puissance pour la même vitesse. Dans mon cas, j'ai pris 20 %.  
J'ai effectué une **remise à zéro** en cliquant sur le logo de la petite maison.   
IL y avait un problème, le bouton status est dès lors rouge. J'ai du repositionner la feuille car celle-ci dépassait des limites. J'ai effectué à nouveau une mise à jour. Cette fois-ci, les statuts étaient verts. 
Il est conseillé de vérifier que l'ensemble du dessin à découper est correctement positionné. Pour se faire, il faut cliquer sur **run boundaries**. Durant cette opération, le laser va se déplacer et simuler son passage. Il faut que le couvercle soit ouvert afin de pouvoir observer.
Si tout est bon, il faut fermer le couvercle et lancer la découpe via le bouton **run**.

### Mise à l'arrêt
Une fois que la découpe est terminée, il y avait beaucoup de fumée. Celle-ci est toxique. J'ai du attendre 15min avant de lever le couvercle. 
Après cela, j'ai éteint la machine via le bouton rouge, le refroidisseur, l'extracteur d'air et fermé l'arrivée d'air comprimé. J'ai ensuite sorti ma feuille découpée. 

### Résultat 
Les réglages étaient bons, la découpe et la gravure se sont bien effectuées. Voici le résultat:  

![découpe](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/d%C3%A9coupe_laser.jpg)

Les différentes possibilités d'ouverture/fermeture de mon abat-jour:

![abat-jour](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/lampepoly.JPG)


# Module 3: Impression 3D

Après avoir modélisé ma chaise dans _Fusion 360_, il était temps de l'imprimer. Pour se faire, j'ai téléchargé le logiciel **[Prusa](https://www.prusa3d.com/drivers/)**. Le logiciel dépend des imprimantes utilisées. 

## Première impression

### Prusa

J'ai donc ouvert les deux fichiers **.stl** que j'avais créés dans Prusa.   
Aprés ça, c'est assez simple, il m'a suffi de régler la taille souhaitée et placer les deux parties à imprimer correctement sur le plateau.  Vu que j'ai importer deux fichiers, pour faire la **mise à l'échelle**, j'ai regarder les facteurs de redimensionnement.  
  
 ![mise à l'échelle](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/pursa%C3%A9chellesmall.png)  

J'ai également ajouté des supports et modifié quelques **règlages**:     
 
  ![réglages impression 3D](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/impression_3D_r%C3%A9glages_strip.png)  

J'ai ensuite suivi ces deux étapes afin d'avoir le **G-code**:
  
  ![Pursa découpe](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/pursastripsmall.png)

### Impression

Une fois le G-code obtenu, je l'ai mis sur la carte SD de l'imprimante. Avant de lancer l'impression, j'ai choisi de changer la couleur du fil. Pour ça, j'ai été dans le menu de la machine **changer filament**. Si celui-ci ne sort pas de lui même, il faut légèrement tirer dessus et ensuite faire rentrter celui de la couleur souhaitée. Il est nécessaire d'attendre un peu avant que la couleur qui sorte soit la bonne.  
Ensuite, j'ai lancer l'impression. Je n'ai eu aucun problème; elle a duré quatre heures. Voici une photo durant l'impression:  

  ![impression 3D](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/impression3Dsmall.JPG)

Après ça j'ai pu enlever les supports et manipuler ma chaise au 1/10.   

![résultat impression 3D](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/impression3D.JPG)  
Des supports étaient présents à l'intérieur du couvercle, le rendant plat du côté intérieur. Cela m'a donné des idées pour revisiter cette assise et pouvoir la transformer en table basse en plaçant le couvecle à l'envers.

## Seconde impression

### Prusa
Une fois le logiciel ouvert, j'y ai glissé mes deux fichiers **stl**. Ensuite, j'ai mis les pièces à l'échelle souhaitée; j'ai préféré mettre à la même taille que la dernière fois. 
Au niveau des réglage, rien à changer par rapport à la fois précédantes sauf le fait que j'ai employé une imprimante avec une buse de **0.6mm**. La hauteur de couche est donc de 0,3mm.  
![réglage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/prusa_r%C3%A9glage_small.jpg)

NB: Il faut être attentif à l'imprimante que l'on utilise. Au départ, je n'avais pas remarqué qu'il s'agissait d'une buse d'une autre taille. Mon premier essai a donc raté. 

Cette fois aussi j'ai généré des supports.  
![supports](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/prusa_gcode_small.png)

Pour finir, j'ai exporter le **G-code**.

### Impression
Une fois mon G-code mis sur la carte SD de l'imprimante, le plateau nettoyé et le fil choisi placé; j'ai pu lancer l'impression. Comme dit plus haut, j'ai du recommencer cette dernière pour cause de mauvais réglages. Une fois ceux-ci modifiés, le lancement c'est bien passé. 

NB: Il faut faire attention à la quantité de fil qu'il reste sur la bobine. De sorte qu'il y en ait assez pour toute l'impression et de ne pas avoir de mauvaises surprises. Cela aurait pu m'arriver si l'on ne m'avait pas fait la remarque. 

Voici une photo de ce début d'impression.  
![photo durant impression](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/IMG_9944.JPG)

Résultat:
![photo résultat](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/IMG_9953.JPG)

J'ai eu un problème pour enlever les suppports. Ceux-ci étant plus résistant que le mécanisme d'ouverture/fermeture, ces derniers sont restés attachés
aux supports et non à l'objet. Il faudrait que je modifie le diamètre du mécanisme afin qu'il soit plus solide. Je devrais égalemment rendre les supports plus légés.



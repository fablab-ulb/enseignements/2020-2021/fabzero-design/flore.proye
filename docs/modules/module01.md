# Module 1: GitLab & Fusion 360

Cette première semaine nous avons appris les bases de deux logiciels: **Fusion 360** qui est un modélisateur 3D  et **GitLab** qui est une platforme d'apprentissage et de partage continue. Ce dernier permet également de garder des traces de l'évolution d'un projet, dans le cadre de ce cours nous allons l'utiliser pour partager nos avancé avec les enseignants et les autres étudiants.   
 Petit conseil, n'hésitez pas à demander de l'aide autour de vous car au début il faut un peu s'accrocher.

## Fusioin 360
Si vous ne connaissez pas ce logiciel, tout comme moi, je vous consielle de [télécharger Fusion 360](https://www.autodesk.com/products/fusion-360/personal). Il vous sera utile pour dessiner en 3D et donc l'impression 3D. Voici un petit [tuto](https://www.youtube.com/watch?v=ExHGnBdvz7o&ab_channel=Crea_Din3D) que j'ai suivi afin de me familiariser à ce programme. 

## GitLab

### Markdown
Markdown est une version simplifiée de **HTML** qui est une syntaxe de formatage de texte brut. Il est utile de connître cette écriture pour créer des documents dans GitLab. Il s'agit d'une forme d'écriture utilisée dans le domaine numérique. Il permet d'écrire de manière plus rapide via des forme de code. Personnelemnt, j'ai eu un peu de mal à saisir le rôle de cette forme d'écriture; j'ai donc regarder [cette vidéo](https://www.youtube.com/watch?v=f49LJV1i-_w&ab_channel=Codecademy) afin d'en saisir l'utilité. J'ai également suivi [ce tutoriel](https://www.markdowntutorial.com/) afin de me familiariser.  Ce qu'il faut en retenir:   
* style d'écriture :
  *   gras: entourer le mot de ** 
  *  italique: entourer le mot de _
  *  gars & itamique: entourer le mot de **_
* En-tête:
  * # Titre de premier ordre = #Titre 
  * ## Titre de second ordre = ##Titre
  * ### Titre de troisième ordre = ###Titre  
  * Et ainsi de suite ...  
 PS: ajouter un espace entre le hashtag et le mot 
* Lien: 
  * inline link: [text] (lien internet)
  * refernece link: pemet de changer facilement le lien présent à plusieur reprises car il se situe en bas de page
* Images: même principe que le lien
  * inline image link: ![nom de l'image] (lien)
* Citation : ajouter > devant la citation qui est entre guillemets. Le résultat sera:   
>"L’architecture commence quand vous mettez en relation deux briques. C’est là que tout commence."   
 Mies Van Der Rohe  

* Liste: 
  * non-ordonnée: placer * suivie d'un espace devant chaque point
  * ordonnée: placer un némuro devant
  * sous-point; ajouter un espacer deavnt le *

### Git Bash
**Git** est un **terminal**, c'est-à-dire un logiciel de commandes qui permet d'effectuer différentes tâches via divers logicels sans avoir besoin de les ouvrir. C'est donc un gain de temps, une fois qu'on ci connait un peu. 
Si comme moi, vous êtes sur un pc et que vous ne disposez pas d'un logiciel de commande, [télécharger Git.](https://git-scm.com/download/win)
**Git Bash ** est la fusion de **Shell Bash** et **Git**. Personnelemnt, je n'avais pas encore Git et donc en l'installant sur mon pc, j'ai pu directement sélectionner Git Bash.Si ce n'est pas votre cas, il est possible de faire cette fusion par après en suivant cette [vidéo.](https://korben.info/installer-shell-bash-linux-windows-10.html?fbclid=IwAR2RLq1CjjgmNsrO9K3jrZoiaHopgs58HVKwRleKp96GGjL_B5PLoobWjhw)

### Clef SSH

Couplée avec GitLab, elle permet d'avoir un accès plus sécurisé à partir de notre ordinateur. Elle permet de s'identifier automatiquement à Git sans avoir à mettre à chaque fois notre identifiant et mot de passe. Il existe plusieurs clefs SSH: la RSA et la ED25519. La **ED25519** SSH clef est plus recommandée étant donné qu'elle est plus sécurisée et performante; c'est donc celle-là que j'ai utilisée.
Afin de créer cette clef, il faut renter les informations suivantes dans le terminal GitBash:
![capture d'écran clef SSH](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/Capture_d_%C3%A9cran_2020-10-20_160859.png)    

Une fois la clef obtenue, deux clefs sont en réalité enregistrées dans votre ordinateur, une publique et une  privée. Les miennes se sont sauvées dans mon disques C, dans le dossier .ssh.   
Il ne reste plus qu'à enregister la clef publique dans votre profil GitLab. Pour se faire, il faut aller dans GitLab et clicker sur votre icône en haut à gauche, et puis sur settings. Ensuite allez dans clef SSH. Il va falloir ouvrir la clef publique qui se trouve sur le disque C de votre pc. Si comme moi vous n'arrivez pas à l'ouvrir du premier coup, il faut l'ouvrir avec le logiciel [Atom](https://atom.io/)(éditeur de texte). Copier et coller le lien dans GitLab et le tour est joué!

### Clonage
Le clonage est une copie de vos fichiers GitLab sur votre ordinateurs. Cela permet par la suite de pouvoir continuer à travailler sur vos documents hors de GitLab. Pour se faire, suivez les étapes suivantes:
 1. Ouvrir GitBash dans le dossier de l'ordinateur où l'on veut avoir accés à tous nos fichiers.
 2. Copier le lien _clone with SSH_.
  ![lien clonage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/clone_lien.png)
 3. Taper la commende suivante dans le terminal.  
  ![commande](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/clone_commande.png)  
 4. C'est déjà fini !

# Module 5: Shaper Origin

Nous avons eu la possibilité de suivre une formation pour la Shaper cette semaine. Voici un résumé des informations à prendre ne compte avant d'employer cette machine.

## Spécifications
 * profendeur max.: 43mm mais cela dépend de la fraise employée
 * diamétre de collet: 8mm ou 1/8"
 * format fichier: SVG

### Précaution d'usage
 * avoir un plan de travail stable
 * fixer le matériau à découper sur le plan de travail: serre joint, fisse ou **double fasse**
 * toujours relever la fraiseuse (bouton rouge) en passant d'une forme à l'autre
 * toujours utiliser l'aspirateur

### Sécurité
 * casque et lunette
 * ne pas travailler à bout de porté
 * être vêtu de manière approprié (pas de vêtement ample, cheveux attachés)

## Paramètres & infos

### Différents types de traits
 * inside
 * outside
 * on line
 * pocket

### Vitesse de fraisage
De 1 à 6: 
 * 1: la plus lente, difficulté à rester droit
 * 6: la plus rapide, risque de surchauffe et de traces de brûlures

### Plusieurs passages
Il peut être utile d'effectuer plusieurs passages pour un meilleur rendu.

### Option offset
Elle peut être utile pour des emboitements de pièces.  
![offset](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/offset.jpg)

### Infos pratiques
![schéma](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/overview.png)

### Changer de fraise
!^[fraise](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/collet.2.png)
1. enlever la prise 
2. dévisser sur le côté
3. bloquer via le bouton gris
4. dévisser à l'aide de la clé

### Placement ShaperTape
Le ShaperTape est une bande adhésive avec des repères qui permettent à la shaper de se situer. Il est nécessiare de les placer dans le champ de vision de la caméra. Il faut qu'il y ait assez de tape et de minimum 4 dominos de long. Si un domino est coupé en deux, il n'est plus utilisable. S'il y a moyen, il est préférable de ne pas couper dans le tape; il peut ainsi être réutilisé. 

## Mise en marche
1. Brancher la prise
2. Scanner
 Il faut scanner toute la surface ou on a mis du tape. Lorsque c'est scanné, le tape devient bleu. Mieu vaut voir l'espace de travail large.
3. Dessiner
 Importer le dessin sous le format **SVG** via une clé usb. Il est également possible de dessiner directement sur l'appareil mais ce n'est pas très précis. Ensuite, il faut placer le dessin sur le plan de travail en appuyant sur le bouton vert.
4. Fraiser 
  Choisir le type de trait (inside, outside, on line) pour chaque partie du dessin. 
  Sélectionner le diamètre de la fraise. Il est nécessaire d'effectuer le z tuch à chaque changement de fraise. Cela permet de savoir à quelle hauteur la fraise se trouve par rapport au plan de travail. 
5. Vérifier notre position
 Le petit domino en haut à droite de l'écran doit rester gris pour l'ensemble du dessin. Si jamais c'est rouge, il faut replacer du tape et scanner à nouveau.
6. Lancement
Il est temps d'allumer la fraise et l'aspirateur. IL ne reste plus qu'à bouger la machine en suivant l'indication de la machine.












 
 

# THE GARDEN EGG CHAIR - revisite

![the garden egg chair](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/Garden_egg_chair.jpg)  
The garden egg chair de **Peter Ghyczy** - 1968

# PROJET FINAL

![personne assise](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/IMG_0456.1.JPG) ![transport](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/IMG_0481.1.JPG)  

Ma réflection a débuté par l'analyse de mon objet de référence et de son designer, Peter Ghyczy. Il avait pour objectif de créer une assise transportable. Cependant, la réalité est autre, the Garden Egg Chair est lourde et peu pratique. C’est d’ailleurs sa forme qui est devenue iconique. Cela m’a poussé à travailler sur une assise regroupant trois concepts : **transportabilité, modularité et courbe**.
Je me suis rapidement dirigée vers le sliceform. Dans un premier temps, j’ai effectué des tests pour déterminer la forme, ensuite le mécanisme de pliage et la matière. Afin d’atteindre mon objectif, mon assise doit être : **pliable, légère, solidaire et résistante**. L’assise conçue prend la forme d’un pouf qui est capable de se replier sur lui-même afin d’être facile à emporter. Il est composé d’une série de pièces qui s’entrecroisent perpendiculairement ; cela crée un objet dynamique, polymorphe pouvant s’adapter à son contexte.

[télécharger le fichier svg](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/projet_final.svg)

# RECHERCHES

## APPROCHE

Avant de nous lancer dans l'exercice final, qui est de réinterpréter, revisiter l'objet choisi, nous devons prendre position sur la manière dont nous voulons le faire. Pour cela, cinq approches nous sont proposées :

 * référence

 * influence

 * hommage

 * inspiration 

 * extension

 Afin de faire notre choix, nous avions à disposition un [fichier](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/blob/document/docs/design_mots_definition.pdf) reprenant les définitions des cinq mots issus de différents dictionnaires. Ceux-ci ont été choisis afin d'avoir des définitions complètes. 

 * Wiktionnaire: constamment remis à jour, libre
 
 * Le Petit Robert: dictionnaire de langue
 
 * Larousse: dictionnaire encyclopédique
 
 * Littré: dictionnaire littéraire du XIXe 

### MON CHOIX
Dans un premier temps, avant de lire les différentes définitions, j'étais certaine de choisir l'inspiration. Ce dernier étant l'approche la plus libre selon moi. 
Après avoir pris plus ample connaissance des démarches, mon intention c'est également portée sur l'extension. 
Voici les définitions de ces deux mots qui m'ont parlées:

Inspiration  

  * Acte de stimulation de l’intellect, des émotions et de la créativité à partir d’une influence. 

  * Souffle créateur qui anime les artistes, les chercheurs. L'inspiration poétique. Attendre l'inspiration. 

  * Action d'inspirer, de conseiller qqch. à qqn ; résultat de cette action.  

Extension  

  * Action d’étendre un corps, de lui faire acquérir plus de surface

  * (Informatique) Programme destiné à rajouter des possibilités supplémentaires à un autre programme. Pour un jeu, on parle plutôt d’addon (ou add-on. Pour un logiciel, on emploie plutôt le mot plug-in.

  * AU FIGURÉ Action de donner à qqch. (déclaration, loi, contrat…) une portée plus générale, la possibilité d'englober un plus grand nombre de choses.

Après mûre réflection, mon choix se porte sur une démarche d'**inspiration**. Celle-ci me correspondant davantage pour sa liberté, créativité et le nombre de possibilités. Parfois, l'absence de contrainte m'est difficile car j'ai du mal à faire des choix afin d'avancer. Je suis néanmoins prête à tenter cette approche. 
L'extension me parraissait intéressente pour essayer de revisiter un objet tout en gardant son essence, son identité. Mais je sais que cette méthode m'aurait parru pesente rapidement par le nombre de limites qui lui sont liées.

## BRAINSTORMING & CONCEPT DE L'INTERPRETATION 

Pour commencer le processus de revisite de mon objet, j'ai écrit une liste de mots en lien avec mon objet, the garden egg chair. 

![brainstorming](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/brainstorming.jpg)

M'étant également plus intéressée à l'approche de Peter Ghyczy, j'ai choisi de seulement garder quelques mots: **modulaire, courbe, transportable**.

Le designer avait pour objectif en créant cette assise qu'elle soit facilement transportable. Néanmoins, après avoir discuté avec une autre étudiante travaillant sur cet objet, j'ai eu confirmation qu'il était peu pratique à manipuler (par sa dimension et le fait d'avoir qu'une poignée) et lourd. C'est exactement l'impression que j'avais eu lors de la visite du Art and Design Atomium Museum. C'est d'ailleurs ce paradoxe entre l'intension du designer et l'image que la création renvoie qui m'intéressait. 

![Peter Ghyczy](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/garden_egg_chair_transpot.jpg)

Peter avait également une approche fonctionnelle; ce n'était pas l'aspect de l'objet mais son utilité qui primait. Cela peut paraitre étrange à première vue car c'est notamment la forme de la garden egg chair qui l'a popularisée. C'est pourquoi je compte travailler avec des courbes; ce point me parait important pour rester connectée à mon choix.

J'ai donc décidé de travailler une assise reposant sur les trois concepts suivants: modulaire, courbe, transportable.

## RECHERCHES

### MOBILIER MODULAIRE, PLIABLE
![recherches modulaire](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/inspi_strip.png)

Dans un premier temps, j'ai effectué quelques recherches sur du mobilier modulaire et pliable afin que cela puisse me donner quelques idées. Bien que ces mécanismes soient intéressants, aucun ne me parle vraiment. Certains me parraissent vus et revus. De plus je souhaite tirer partie au maximum des possibilités offertent par les machines du FabLab; que l'emploi de celles-ci ait du sens.


### MOBILIER CARTON & DECOUPE LASER
![recherches carton/laser](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/inspi2_strip.jpg)

J'ai décicidé de me positionner par rapport à quelle machine employer afin d'orienter mes recherches. Etant donné la dimension d'une assise et le fait que je ne souhaite pas être restreinte par les matériaux, la **découpeuse laser** me semble être l'idéal. J'en suis arrivée à regarder pour des meubles conçus à l'aide de la dépoupe laser. Un type de meuble m'a tout de suite plu par leur esthétique qui est la mise en avant de l'utilisation de la machine. Je parle des pièces constituées par l'assemblage de multiples planches entrecroisées, **sliceform**. 
Cependant, l'ensemble de ces meubles semblent figés et donc non transportables facilement à moins de les démonter.


### SLICEFORM PAPIER 
![recherches sliceform](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/inspi3_strip.jpg)

Je me suis dit qu'il devait surement y avoir un moyen de replier la forme sur elle-même. Lorsque qu'une armoire n'est pas contreventée, elle peut facilement balsculer, il y a du jeu. C'est ce même mouvement que je voudrais reproduire au point que l'objet se rabatte.   
Suite à plus de recherches, je me suis penchée vers les sliceforms en papier comme moyen d'expérimentation car c'est l'esthétique que je cherche à avoir et que ceux-ci sont pliable. Il existe déjà une diversité d'exemple de forme. Etant intéressée par les formes rondes et aplaties, j'ai trouvé et suivi le [tutoriel : sliceform papercraft 3D torus](https://www.youtube.com/watch?v=3B6qEtNIVvw&ab_channel=DeepTech). Voici le résultat de  ma première maquette d'étude :

![recherche maquette papier](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test_1_papier.JPG) 


## TESTS

### 1. FORME

![test 1 polypropylène](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test_1_poly.JPG)

| informations techniques |  |
| ------ | ------ |
| matière | polypropylène |
| dimension | Ø 13,5 cm |
|  | hauteur: 5,5 cm  |
| épaisseur matière | 0,7 mm |
| entailles| 0,7 mm |
| paramètre laser | F: 1500|
|  | % 46|

Dans un premier temps, j'ai fait un test pour tester le mécanisme et le concept, une assise pliable. Le choix de la matière m'important peu à ce stade, j'ai choisi d'employer des feuilles de polypropylène par facilité: disponible au FabLab et même matière que la précédente utilisation de la découpeuse laser.

#### PROBLEME FICHIER
Voici un apperçu des différentes pièces nécessaire à la construction:   

![plan découpe](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/patron.png)  
Dans un premier temps, j'ai fait un test pour tester le mécanisme et le concept, une assise pliable. 
J'ai rencontré énormément de problèmes lorsque j'ai passé le fichier sur l'interface DriveboardApp. J'ai employé le document que j'avais dessiné avec AutoCad pour la maquette papier mais à plus grande échelle. Je l'avais exporté en pdf, réouvert dans Illustrator afin de le sauver en svg pour pouvoir l'ouvrir dans Inkscape (où j'ai ungroupé le fichier). Malgré avoir suivi le même protocole que lors de la formation, le fichier se modifie automatiquement (forme et échelle). Voici un exemple de l'un des problèmes:

![problème fichier](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/probl%C3%A8me_doc.jpg)

Après avoir recommencé à passer pas mal de temps à voir si le souci ne venait pas du passage dans Inkscape, j'ai repris le fichier du point de départ. J'ai modifié à plusieurs reprises le document à partir d'AutoCad (en créant des groupes, en vérifiant qu'il n'y ait pas de double trait, en modifiant l'échelle) et j'ai commencé à perdre espoir. J'ai tenté en exportant le fichier d'AutoCad et DXF afin de passer par moins de conversions de formats. On peut dès lors l'ouvrir directement dans Inkscape.
Rien à faire, malgré l'aide des assistantes je ne trouve pas le problème. J'ai donc séparé mon fichier en plusieurs parties et n'ai découpé que les bonnes pièces. Les pièces rencontrant des problèmes étant peu nombreuses, cela n'a pas impacté la maquette.

#### ECHEC
Une fois que j'ai enfin découpé la majorité des formes et que j'ai commencé à assembler l'ensemble, je me suis rapidement rendu compte qu'il y avait un problème de dimensionnement des fentes. Effectivement, sachant que l'épaisseur de la découpe du laser était suffisante pour une entaille (dû à l'exercice lors de la formation) je ne me suis pas posée de question; la multiplication de celles-ci pose problème.   
Ce test fut un échec, je vais dès lors faire attention à la largeur des fentes.

### 2. FORME

![test 2 carton](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test_2_carton.JPG)

| informations techniques |  |
| ------ | ------ |
| matière | carton brun|
| dimension | Ø 16,6 cm |
|  | hauteur: 6,7 cm  |
| épaisseur matière | 3 mm |
| entailles| 3,5 mm |
| paramètre laser | F: 1200|
|  | % 40|

Pour ce second essai, j'ai choisi d'utiliser du carton brun pensant éventuellement faire le prototype en cette matière ayant comme avantage d'être légère, solide et facilement découpable à la laser.   
Je n'ai pas rencontré de problème lors de cette découpe et de l'assemblage. Cependant, après réflection en observant la forme, je pense qu'il est nécessire de mofifier celle-ci étant trop ronde et faisant référence à un donut; ce qui peut être amusant mais n'est pas l'objectif souhaité. Elle est aussi peu pratique pour une assise vu le trou. 


### 3. FORME

![test 3 carton](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test_3_strip.jpg)

| informations techniques |  |
| ------ | ------ |
| matière | carton brun|
| dimension | Ø 16,6 cm |
|  | hauteur: 6,7 cm  |
| épaisseur matière | 3 mm |
| entailles| 3,5 mm |
| paramètre laser | F: 1200|
|  | % 40|

J'ai retraivaillé cette version sur base de la maquette précédente. J'ai découpé quelques pièces afin de changer celle du centre de la version antérieure rendant ainsi la forme fermée. La forme obtenue me plait bien, elle invite à s'assoir.  
Néanmoins, dans cet essai, on ne retrouve pas la possibilité de pliage qui est présente dans le test en papier. Cela est du au fait qu'il n'y a pas de jeux dans l'imbrication des différentes parties. Toutefois, l'ensemble est très résistant.

![test 3 gif pliage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_5fda01b01d2e5.gif)  ![test 3 gif tenue](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_5fda02d5cb7b6.gif)

### 4. PLIAGE

![test 4 carton](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test_4_strip.jpg)

| informations techniques | |
| ------ | ------ |
| matière | carton brun |
| dimension | Ø 18,5 cm |
|  | hauteur: 6,9 cm |
| épaisseur matière | 2,5 mm |
| entailles| 6 mm |
| paramètre laser | F: 1200|
|  | % 40|

Pour cette version, j'ai voulu rendre le tout pliable. Pour se faire, j'ai travaillé sur la taille des entailles. Avant de me lancer sur AutoCad et de modifier pièce par pièce les dimensions, j'ai effectué quelques recherches afin de trouver un logiciel qui "slice" des objets 3D. A partir de là, j'emploie [**Slicer for Fusion 360**](https://knowledge.autodesk.com/fr/support/fusion-360/downloads/caas/downloads/downloads/FRA/content/slicer-for-fusion-360.html).  

![slicer for fusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/slicer_for_fusion.png)

Ce programme est simple à prendre en main, après avoir suivi le [tutoriel: Making: 3D Model to 2D Vector](https://www.youtube.com/watch?v=a1MKnQkca3Y&ab_channel=JoelHall) j'ai pu l'utiliser sans problème. Il permet différents styles d'assemblages, de modifier la taille des fentes, d'autres paramètres et surtout de produire les plans nécessaires à la découpe sous format DXF. Il n'y a plus qu'à aller dans Autocad afin de bien positionner le tout et ensuite dans Inkscape.

Le résultat de cette tentative est positif, il y a un mouvement de **replie** mais ce dernier n'est pas encore optimal.
Toutefois, un nouveau **problème** se pose: la **désolidarisation du tout**. Lorsque je souhaite soulever mon objet sans le plier avant, l'ensmble se détache très facilement. 

![test 4 gif](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_5fda058065168.gif)

### 5. PLIAGE & MATIERE

![test 5 polypropylène alvéolaire](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test_5_strip.jpg)

| informations techniques |  |
| ------ | ------ |
| matière | polypropylène alvéolaire |
| dimension | Ø 37 cm |
|  | hauteur: 13,8 cm |
| épaisseur matière | 3,5 mm |
| entailles| 8 mm |
| paramètre laser | F: 1200|
|  | % 50|

Le carton n'étant pas résistant dans le temps, j'ai testé de nouvelles matières. Le polypropylène alvéolaire a les mêmes avantages que le carton: résistant, léger mais est également moins endommageable dans la durée. J'ai dû un peu rechercher avant de pouvoir en acheter en petite quantité, ces plaques étant principalement utilisées comme intercalaire dans les palettes ou comme support pour la publicité. J'ai fini par en trouver chez un imprimeur. De plus, je trouve intéressant de mettre en valeur des matériaux généralement employés pour leurs qualités techniques et non esthétiques.  
Ce test a été réalisé à l'échelle 1/2, voulant être plus proche des dimensions réelles pour analyser les problèmes possibles.  
Le résultat étant un objet pliable mais peu pratique étant donné qu'il y a toujours le problème de désolidarisation des parties.

![test 5 gif](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_5fda066ac27d3.gif)

Afin de remédier à ce problème, j'ai tenté de rendre le tout inséparable gràce à du fil. Commençant par percer des trous à l'aide d'une aiguille chauffée dans l'ensemble des pièces: dans le profil pour une moitié et sur la face pour l'autre. Ensuite, je me suis amusée à passer un fil (test: en nylon et en fer) dans la longueur des pièces. Le processus étant laborieux, j'ai changé de méthode en attachant que les extrémités.

![test 5 couture](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test_5_couture_strip.jpg)

En conclusion, ce procédé est long et la mise en oeuvre pas simple mais ça fonctionne.

![test 5 tenue réussite](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_5fda56f15cea8.gif)

### 6. PLIAGE & MATIERE & DIMENSION

![test 6 polypropylène](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test_6_strip.jpg)

| informations techniques |  |
| ------ | ------ |
| matière | polypropylène |
| dimension | Ø 18,5 cm |
|  | hauteur: 7,5 cm  |
| épaisseur matière | 0,7 mm |
| entailles| 2 mm |
| paramètre laser | F: 1500|
|  | % 45|

Pour cet essai, j'ai légèrement modifié les proportions de l'assise afin que la hauteur soit plus confortable. La dimension de celle-ci à taille réelle est de **74 cm de diamètre pour une hauteur de 30cm**.  

Après réflexion, je réalise que la **matière ne doit pas nécessairement être inflexible** étant donné que mon essai en papier était solide. En effet, la multitude d'enboitements rend le tout plus rigide.     
Je suis repartie sur un test de feuille en polypropylène. L'ensemble est bien rigide mais à voir jusqu'à quel point; un test à échelle réelle serait nécesssaire à déterminer le poids maximum pouvant être exercé. Vu la finesse de la planche employée, il ne m'est pas possible de reproduire le même principe que ci-dessus pour solidariser l'ensemble. Il me faut donc trouver une nouvelle méthode pour résoudre ce problème, si je continue là-dessus.  

![test 6 gif pliage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_5fda3f598879b.gif)  ![test 6 gif tenue](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_5fda3f81696af.gif)


### 7. PLIAGE & MATIERE 

![test 7 molleton](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test_7_strip.jpg)

| informations techniques |  |
| ------ | ------ |
| matière | molleton rigide Jeffytex |
| dimension | Ø 18,5 cm |
|  | hauteur: 7,5  |
| épaisseur matière | 3,5 mm |
| entailles| 4,5 mm |
| paramètre laser | F: 1500|
|  | % 15|
| temps de découpe | 35 min |
| temps d'assemblage | 3 min |

Trouvant le plastique peu "accueillant", j'avais envie de tester cette assise avec une matière plus chaude, plus attrayante. J'avais en tête d'essayer la feutrine, celle-ci étant néanmoins très souple, bien que l'assemblage renforce les capacités des matériaux dans une certaine mesure. Voulant assurer un minimum de soutien, je me suis dirigée vers du **molleton rigide**. Ce dernier a les mêmes composants que la feutrine mais est rigide et peut donc être découpé au laser.     
J'ai dû effectuer quelques tests pour trouver les paramètres adéquats du laser. L'idéal auquel je suis arrivée est une vitesse **F = 1500** et puissance de **15%**.  
Vu le test précédent, je me suis dit qu'il devait être possible de trouver l'équilibre idéal entre la taille des fentes afin que l'objet soit pliable et solidaire; comme pour le papier.  
Le résultat est bien là. De plus, cette matière a tendance à s'accrocher ce qui rend le tout très uni. La maquette est **rigide, pliable, solidaire et légère**.


![test 7 gif pliage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_5fda07a5326b4.gif)  ![test 7 gif tenue](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_5fda07dc1e1cf.gif)

### 8. ECHELLE 1/1 & FORME

![test 8 polypropylène alvéolaire](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test8_strip.JPG)

| informations techniques | |
| ------ | ------ |
| matière | polypropylène alvéolaire |
| dimension | Ø 65 cm |
|  | hauteur: 30 cm  |
| épaisseur matière | 3,5 mm |
| entailles| 8 mm |
| paramètre laser | F: 1200|
|  | % 50|
| temps de découpe | 2h |
| temps d'assemblage | 5 min |

Pour ce premier test à l'échelle 1/1, j'ai choisi le polypropylène alvéolaire. Ayant déjà testé cette matière précédemment, je n'aurai pas de mauvaises surprises. De plus, ça m'a permis de tester la forme, le confort de l'assise en dépensant moins d'argent qu'un test en molleton. En passant à cette échelle, j'ai modifié la forme du pouf en le rendant **plein en partie basse** de sorte d'améliorer la structure.

Lors de la découpe, j'ai eu quelques soucis de fichiers. La grande majorité des fichiers ont été bons directement, néanmoins certains une fois ouverts sur la platforme DriveboardApp changeaient d'échelle. Afin de remédier à ça j'ai dû grouper les fichiers dans Autocad, l'exporter en dxf, l'ouvrir dans Inkscape pour le sauver en svg. Après ça, je n'ai plus eu de problèmes. 
La découpe à cette échelle a pris du temps étant donné que la ventilation de la machine n'est pas optimale, il est nécessaire d'attendre entre chaque planche 15 minutes. J'avais une douzaine de plaques à faire. Au final, je pense en avoir eu pour 2h de découpe sans compter le temps d'attente entre. 

Le montage fut simple, il n'a fallu que quelques minutes. Ce qui a pris un peu plus de temps ce fut de trier les pièces afin de les avoir dans le bon ordre et du bon côté.  Afin que les pièces soient **solidaires** entre elles, j'ai passé un fil à travers celle-ci dans leurs extrémités (voir le test 5).


![test 8 gif pliage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_6008691405f63.gif)

![personne assise](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/IMG_0456.1.JPG) ![transport](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/IMG_0481.1.JPG)

En testant le pouf, j'ai été positivement surprise par son **confort**. On est vraiment bien assis dedans, la hauteur est également agréable. De plus, il est **solide**; il peut supporter jusqu'à 120kg. Il est **pliable** facilement et ne prend que peu de place, on peut aisément le transporter d'un endroit à un autre.  

En le manipulant, on peut lui donner **diverses formes**. C'est un objet dynamique qui peut s'adapter, se mouvoir dans dans les deux directions. 

#### MODIFICATION DU FICHIER

Afin d'avoir une découpe plus fidèle au modèle, des courbes bien dessinées, il est nécessaire d'ouvrir le fichier depuis Fusion 360 et non de l'exporter pour ensuite l'ouvrir dans Slicer for Fusion 360. Cela permet de choisir la définition de l'objet.

![importation d'un modèle dans slicer](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/fusion_to_slicer.1.png)  

### 9. ECHELLE 1/1 & MATIERE

![test 9 molleton](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/test9_strip.JPG)  

| informations techniques |  |
| ------ | ------ |
| matière | molleton rigide Jeffytex |
| dimension | Ø 60 cm |
|  | hauteur: 28 cm |
| épaisseur matière | 3,5 mm |
| entailles| 4,5 mm |
| paramètre laser | |
| Vitesse | 100% |
| Puissance | 75% |
| temps de découpe | 40 min |
| temps d'assemblage | 15 min |

La découpeuse laser du FabLab étant tombée en panne, une **nouvelle machine** a été mise à disposition. Celle-ci est plus petite 80x50cm, j'ai donc réduit légèrement la taille de mon objet afin de pouvoir l'employer sans gaspiller de matière. Après avoir refait mes fichiers à la bonne taille et séparé les éléments (un seul grand fragrment par planche), j'ai pu commencer la découpe. En assemblant les premières pièces ensemble, je me suis rendue compte que les entailles étaient plus larges que dessinées (5,5mm). Cela posait problème étant donné que les pièces adhères moins bien l'une à l'autre; ce qui est l'avantage de cette matière. 
J'ai recommencé mes documents en réduisant la largeur des entailles à 3,8 mm afin qu'une fois coupées elles soient actuellement à 4,5 mm. 

Le molleton est vendu en rouleau ce qui a comme conséquence qu'il est **courbé** et non plat. Lors de la découpe, j'ai collé ce dernier à une plaque de polypropylène et posé des poids dessus pour l'aplatir. Lors de l'assemblage, cela à également posé problème.

![découpe](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/IMG_0458.1.jpg)  

 Ma première tentative de montage étant complexe du au fait que les pièces courbaient dans tous les sens. J'ai du rendre chaque segment de l'assise lisse. Pour se faire, j'ai repassé à une partie des pièces à basse température. Une autre méthode que j'ai également testée est de placer deux ou trois pièces à la fois sur un chauffage avec des poids. Les deux techniques donnent le même résultat. 

Une fois que mes pièces étaient prêtes, j'ai pu commencer l'assemblage. Ce dernier m'a pris plus de temps que d'habitude; la matière de chaque partie s'attachant l'une à l'autre.

![test 9 gif pliage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_60086be75e34e.gif)
![test 9 gif tenue](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/bloggif_60086b737dcbb.gif)

![personne assise](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/IMG_0467.1.jpg)

En testant l'assise, je me suis rapidement rendue compte qu'elle n'était pas aussi solide que la précédente. Le molleton a tendance à légèrement se plier sous le poids d'une personne assise. Néanmoins, j'ai pu m'assoir dedans sans problème, elle résiste à mon poids mais je doute qu'elle puisse en supporter plus. 
Pour ce qui est du pliage et de la flexibilité de la forme, cela fonctionne bien. Elle se replie sur elle-même sans problème, elle est légère et maniable. 

Néanmoins, afin de rendre cette dernière plus solide, j'ai appliqué de la colle sur les extrémités du côté supérieur et sur une seule face. Dans un premier temps, jai testé sur un échantillon de la **colle naturelle faite maison** (si vous voulez en savoir plus, lisez la partie sur la matière). Une fois séchée, la colle a rendu le molleton **davantage rigide**. Faute de temps, je n'ai pu appliquer cette méthode qu'à une partie du prototype. Je vois la différence, l'assise résiste un peu mieux. Néanmoins, je pense qu'étendre cette technique à l'ensemble du projet ne peut être que bénéfique.
Différentes solutions peuvent permettre d'améliorer la solidité: la même matière plus épaisse, augmenter le nombres de pièces formant l'assise, traiter la matière actuelle avec de la colle. 

En conclusioin, cette version est **transportable, modulaire, légère**, les pièces sont **solidaires**. Il reste un peu de travail afin qu'elle soit assez solide pour que quiconque puisse en profiter.

#### PARAMETRES DE LA NOUVELLE DECOUPEUSE LASER

Cette machine étant moins puissante mais plus rapide que la grande, j'ai du faire de nouveaux tests pour trouver les bons réglages afin de couper le molleton. Cette matière pouvant s'enflammer rapidement j'ai commencé par une **vitesse de 100%** et une puissance de 5 %. Montant progressivement, je suis arrivée à une **puissance de 75 %**. Les autres paramètres ne doivent pas être changés.

![paramètres](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/param%C3%A8tres.jpg)

## MONTAGE

[vidéo montage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/img-0298-bjfdfxaq-33ak-kt0kbjo2-runkpq83-8l8wsn4c-srlt-199xx2gl-jceirvai_eh7E9pjk.mov)

## MATIERE

![matière photo](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/mati%C3%A8re.1.jpg)  

### POLYPROPYLENE ALVEOLAIRE

Le polypropylène alvéolaire a les mêmes avantages que le carton: résistant, léger mais est également moins endommageable dans la durée. Ces plaques sont principalement utilisées comme intercalaire dans les palettes ou comme support publicitaire. De plus, je trouve intéressant de mettre en valeur des matériaux généralement employés pour leurs qualités techniques et non esthétiques. Il est possible d'en trouvé de récupération mais il est important d'être certain qu'il s'agit bien de polypropylène et non d'un autre plastique pour pouvoir le découper au laser.

### MOLLETON RIGIDE JEFFYTEX

Le molleton rigide Jeffytex a un aspect similaire à la feutrine mais est rigide. Il reste néanmoins souple lorsqu'on le manipule. Cette matière est composée de 100% polyester, a un poids de 350 g/m2. Il peut être lavé en machine et repassé à basse température. Il est généralement employé pour des fonds de sac. Il s'agit donc d'un matériau qui à l'origine est plutôt technique. Cela explique probablement pourquoi il est uniquement disponible en blanc (à ma connaissance).

Il est possible de teindre ce textile, voici quelques exemples.  
![teintures](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/couleurs.1.jpg)  
 
#### FAIRE DE LA FEUTRINE SOI-MEME

Il est possible de faire de la feutrine soi-même et de la rigidifier par la suite. Pour se faire, il faut passer à la machine de vieux pulls en laine. Vous pouvez suivre ce [tutoriel](https://www.petitcitron.com/techniques-de-couture/fabriquer-de-la-feutrine-a-partir-d-un-vieux-pull)  pour plus d'informations. Et ensuite les traiter avec de la colle naturelle. 
J'ai fait de la colle naturelle à base de maïzenna lors de mon dernier test. J'ai suivi cette [vidéo](https://www.youtube.com/watch?v=EcIlHgkpfYg&ab_channel=ABCFacileaFaire). Il suffit de mélanger 3 dozes d'eau pour une de maïzenna, de chauffer le tout au micro-onde (par période de 10sec) afin d'obtenir une texture gélatineuse. 

![colle](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/raw/master/docs/images/colle.jpg)  

## PLUS DE POSSIBILITES

Le principe de mouvement de cette assise peut être appliqué à d'autres styles de pièces de mobilier; un fauteuil, un banc, une table basse,... Ce qui est important c'est qu'il y ait assez de masse pour supporter le poids dû à un usage normal. 
Il est également possible de varier les couleurs. 


[A4 jury](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/flore.proye/-/blob/1fc9ba0b0401d2df161781709ae9575a30e925dc/docs/A4_jury.pdf)








